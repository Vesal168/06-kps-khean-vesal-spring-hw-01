/*
 Navicat Premium Data Transfer

 Source Server         : Connection
 Source Server Type    : PostgreSQL
 Source Server Version : 110002
 Source Host           : localhost:5432
 Source Catalog        : Book_Management_System_Db
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110002
 File Encoding         : 65001

 Date: 30/06/2020 23:38:17
*/


-- ----------------------------
-- Sequence structure for tbl_book_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_book_id_seq";
CREATE SEQUENCE "public"."tbl_book_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_category_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_category_id_seq";
CREATE SEQUENCE "public"."tbl_category_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_roles_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_roles_id_seq";
CREATE SEQUENCE "public"."tbl_roles_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tbl_user_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tbl_user_id_seq";
CREATE SEQUENCE "public"."tbl_user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for tbl_book
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_book";
CREATE TABLE "public"."tbl_book" (
  "id" int4 NOT NULL DEFAULT nextval('tbl_book_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default",
  "author" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "thumbnail" varchar(255) COLLATE "pg_catalog"."default",
  "category_id" int4
)
;

-- ----------------------------
-- Records of tbl_book
-- ----------------------------
INSERT INTO "public"."tbl_book" VALUES (2, 'Hello', 'Piseth', 'Dancer', 'sfsdf', 3);
INSERT INTO "public"."tbl_book" VALUES (3, 'Rapper', 'Chantrea', 'Singer', 'abc', 4);
INSERT INTO "public"."tbl_book" VALUES (4, 'Programmer', 'SengLeng', 'Idol', 'Helo', 1);
INSERT INTO "public"."tbl_book" VALUES (5, 'Joava', 'lolo', 'solos', 'adsdfads', 8);

-- ----------------------------
-- Table structure for tbl_category
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_category";
CREATE TABLE "public"."tbl_category" (
  "id" int4 NOT NULL DEFAULT nextval('tbl_category_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of tbl_category
-- ----------------------------
INSERT INTO "public"."tbl_category" VALUES (1, 'Piseth');
INSERT INTO "public"."tbl_category" VALUES (2, 'Piseth');
INSERT INTO "public"."tbl_category" VALUES (3, 'Piseth');
INSERT INTO "public"."tbl_category" VALUES (4, 'Piseth');
INSERT INTO "public"."tbl_category" VALUES (5, 'Piseth');
INSERT INTO "public"."tbl_category" VALUES (8, 'Piseth');

-- ----------------------------
-- Table structure for tbl_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_roles";
CREATE TABLE "public"."tbl_roles" (
  "id" int4 NOT NULL DEFAULT nextval('tbl_roles_id_seq'::regclass),
  "role" varchar COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of tbl_roles
-- ----------------------------
INSERT INTO "public"."tbl_roles" VALUES (1, 'Admin');
INSERT INTO "public"."tbl_roles" VALUES (2, 'User');

-- ----------------------------
-- Table structure for tbl_user_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_user_roles";
CREATE TABLE "public"."tbl_user_roles" (
  "user_id" int4 NOT NULL,
  "role_id" int4 NOT NULL
)
;

-- ----------------------------
-- Records of tbl_user_roles
-- ----------------------------
INSERT INTO "public"."tbl_user_roles" VALUES (1, 1);
INSERT INTO "public"."tbl_user_roles" VALUES (2, 2);
INSERT INTO "public"."tbl_user_roles" VALUES (1, 2);

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."tbl_users";
CREATE TABLE "public"."tbl_users" (
  "id" int4 NOT NULL DEFAULT nextval('tbl_user_id_seq'::regclass),
  "full_name" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "status" bool NOT NULL
)
;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO "public"."tbl_users" VALUES (2, 'User', 'user@gmail.com', '123', 't');
INSERT INTO "public"."tbl_users" VALUES (1, 'Admin', 'admin@gmail.com', '123', 't');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."tbl_book_id_seq"
OWNED BY "public"."tbl_book"."id";
SELECT setval('"public"."tbl_book_id_seq"', 2, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."tbl_category_id_seq"
OWNED BY "public"."tbl_category"."id";
SELECT setval('"public"."tbl_category_id_seq"', 6, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."tbl_roles_id_seq"
OWNED BY "public"."tbl_roles"."id";
SELECT setval('"public"."tbl_roles_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."tbl_user_id_seq"
OWNED BY "public"."tbl_users"."id";
SELECT setval('"public"."tbl_user_id_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table tbl_book
-- ----------------------------
ALTER TABLE "public"."tbl_book" ADD CONSTRAINT "tbl_book_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table tbl_book
-- ----------------------------
ALTER TABLE "public"."tbl_book" ADD CONSTRAINT "category_id" FOREIGN KEY ("category_id") REFERENCES "public"."tbl_category" ("id") ON DELETE NO ACTION ON UPDATE CASCADE;

-- ----------------------------
-- Primary Key structure for table tbl_category
-- ----------------------------
ALTER TABLE "public"."tbl_category" ADD CONSTRAINT "tbl_category_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tbl_roles
-- ----------------------------
ALTER TABLE "public"."tbl_roles" ADD CONSTRAINT "tbl_roles_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tbl_user_roles
-- ----------------------------
ALTER TABLE "public"."tbl_user_roles" ADD CONSTRAINT "tbl_user_roles_pkey" PRIMARY KEY ("user_id", "role_id");

-- ----------------------------
-- Foreign Keys structure for table tbl_user_roles
-- ----------------------------
ALTER TABLE "public"."tbl_user_roles" ADD CONSTRAINT "1" FOREIGN KEY ("user_id") REFERENCES "public"."tbl_users" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE "public"."tbl_user_roles" ADD CONSTRAINT "2" FOREIGN KEY ("role_id") REFERENCES "public"."tbl_roles" ("id") ON DELETE CASCADE ON UPDATE NO ACTION;

-- ----------------------------
-- Primary Key structure for table tbl_users
-- ----------------------------
ALTER TABLE "public"."tbl_users" ADD CONSTRAINT "tbl_user_pkey" PRIMARY KEY ("id");
