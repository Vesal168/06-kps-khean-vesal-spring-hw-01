package com.example.demo.Rest;

import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Repository.Dto.CategoryDto;
import com.example.demo.Service.CategoryService;
import com.example.demo.Utilities.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryRestController {
    private CategoryService categoryService;
    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @RequestMapping (method= RequestMethod.GET)
    public Map<String, Object> getAllData(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                          @RequestParam(value = "limit", required = false, defaultValue = "4") int limit){
        Pagination pagination = new Pagination(page, limit);
        pagination.setPage(page);
        pagination.setLimit(limit);

        pagination.setTotalCount(categoryService.countAllCategory());
        pagination.setTotalPages(pagination.getTotalPages());
        Map<String, Object> result = new HashMap<>();
        List<CategoryDto> categoryDtos = categoryService.getAllData(pagination);
        if(categoryDtos.isEmpty()){
            result.put("Message", "No data in Database");
        }else {
            result.put("Message", "Get Data Successful");
        }
        result.put("Pagination", pagination);
        result.put("Data", categoryDtos);
        return result;
    }
    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> insertCategory(@RequestBody CategoryDto categoryDto){
        Map<String,Object> result=new HashMap<>();
        categoryService.insert(categoryDto);
        result.put("Message","Insert Data successfully");
        result.put("DATA",categoryDto);
        return result;
    }
    @RequestMapping(value = "{id}",method=RequestMethod.PUT)
    public Map<String,Object> updateCategory(@RequestParam int code,@RequestBody CategoryDto categoryDto){
        Map<String,Object> result=new HashMap<>();
        CategoryDto categoryDto1=categoryService.findCode(code);
        if(categoryDto1==null){
            result.put("Message","ID Not Found Update Unsuccessfully");
            result.put("Response Code",404);
        }else{
            categoryService.update(categoryDto);
            result.put("Message","Update Data Successfully");
            result.put("Response Code",200);
            result.put("DATA",categoryDto);
        }
        return result;
    }
    @RequestMapping(value = "{id}",method=RequestMethod.DELETE)
    public Map<String,Object> deleteCategory(@RequestParam int id){
        Map<String,Object> result=new HashMap<>();
        CategoryDto categoryDto=categoryService.findCode(3);
        if(categoryDto==null){
            result.put("Message","ID Not Found Delete Unsuccessfully");
            result.put("Response Code",404);
        }else{
            categoryService.delete(id);
            result.put("Message","Delete Data Successfully");
            result.put("Response Code",200);
        }
        return result;
    }
}
