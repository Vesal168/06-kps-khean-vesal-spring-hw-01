package com.example.demo.Rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@RestController
public class FileUploadRestController {

    @PostMapping("/upload")
    public String upLoadFile(@RequestParam("file") MultipartFile file){
        File uploadedFile = new File("E:\\HRD Center\\Subject\\Spring\\Homeworks\\06-KHEAN-VESAL-KPS-HW-01\\Book_Management\\src\\main\\resources\\Image\\"+file.getOriginalFilename());

        try {
            uploadedFile.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(uploadedFile);
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "localhost:8080/image/"+uploadedFile.getName();
    }
}
