package com.example.demo.Rest;

import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Service.BookService;
import com.example.demo.Utilities.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/book")
public class BookRestController {
    private BookService bookService;
    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping (method=RequestMethod.GET)
    public Map<String, Object> getAllData(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                                          @RequestParam(value = "limit", required = false, defaultValue = "4") int limit){
        Pagination pagination = new Pagination(page, limit);
        pagination.setPage(page);
        pagination.setLimit(limit);

        pagination.setTotalCount(bookService.countAllBook());
        pagination.setTotalPages(pagination.getTotalPages());
        Map<String, Object> result = new HashMap<>();
        List<BookDto> bookDtos = bookService.getAllData(pagination);
        if(bookDtos.isEmpty()){
            result.put("Message", "No data in Database");
        }else {
            result.put("Message", "Get Data Successful");
        }
        result.put("Pagination", pagination);
        result.put("Data", bookDtos);
        return result;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Map<String,Object> insertBook(@RequestBody BookDto bookDto){
        Map<String,Object> result=new HashMap<>();
        bookService.insert(bookDto);
        result.put("Message","Insert Data successfully");
        result.put("DATA",bookDto);
        return result;
    }
    @RequestMapping(value = "{id}",method=RequestMethod.PUT)
    public Map<String,Object> updateBook(@RequestParam int code,@RequestBody BookDto bookDto){
        Map<String,Object> result=new HashMap<>();
        BookDto bookDto1=bookService.findCode(code);
        if(bookDto1==null){
            result.put("Message","ID Not Found Update Unsuccessfully");
            result.put("Response Code",404);
        }else{
            bookService.update(bookDto);
            result.put("Message","Update Data Successfully");
            result.put("Response Code",200);
            result.put("DATA",bookDto);
        }
        return result;
    }
    @RequestMapping(value = "{id}",method=RequestMethod.DELETE)
    public Map<String,Object> deleteBook(@RequestParam int id){
        Map<String,Object> result=new HashMap<>();
        BookDto bookDto1=bookService.findCode(1);
        if(bookDto1==null){
            result.put("Message","ID Not Found Delete Unsuccessfully");
            result.put("Response Code",404);
        }else{
            bookService.delete(id);
            result.put("Message","Delete Data Successfully");
            result.put("Response Code",200);
        }
        return result;
    }
    @RequestMapping(value = "{id}",method = RequestMethod.GET)
    public Map<String,Object> findOne(@RequestParam int id){
        Map<String,Object> result=new HashMap<>();
        BookDto bookDto=bookService.findOne(id);
        if(bookDto==null){
            result.put("Message","ID Not Found");
            result.put("Response Code",404);
        }else{
            bookService.findOne(id);
            result.put("data",bookDto);
            result.put("Message","Find Data Successfully");
            result.put("Response Code",200);
        }
        return result;
    }
    @RequestMapping(value = "{title}", method = RequestMethod.GET)
    public Map<String, Object> searchBookByTitle(@RequestParam String title){
        Map<String, Object> result = new HashMap<>();
        BookDto bookDto = bookService.SearchBookById(title);
        if (bookDto==null){
            result.put("Message", "Title of Book Not Found");
            result.put("Response Code", "404");
        }else{
            bookService.SearchBookById(title);
            result.put("Data", bookDto);
            result.put("Message", "Search Title of Book Successfully");
            result.put("Response Code", "200");
        }
        return result;
    }
}
