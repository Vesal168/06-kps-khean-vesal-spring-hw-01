package com.example.demo.Configuration;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
@Configuration
public class JdbcConfiguration {
    @Bean
    public DataSource myPostgresDb(){
        DataSourceBuilder dataSource = DataSourceBuilder.create();
        dataSource.driverClassName("org.postgresql.Driver");
        dataSource.url("jdbc:postgresql://localhost:5432/Book_Management_System_Db");
        dataSource.username("postgres");
        dataSource.password("1234");
        return dataSource.build();
    }
}
