package com.example.demo.Repository;

import com.example.demo.Model.RoleModel;
import com.example.demo.Model.UserModel;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {
    @Select("SELECT * from tbl_users WHERE email like #{email}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "FullName", column = "full_name"),
            @Result(property = "roles", column = "id", many = @Many(select = "FindRolesByUserId"))
    })
    UserModel loadUserByEmail(String email);
    @Select("SELECT r.id, r.role FROM tbl_roles r INNER JOIN tbl_user_roles ur ON ur.role_id=r.id WHERE ur.user_id=#{id}")
    List<RoleModel> FindRolesByUserId(int id);
}
