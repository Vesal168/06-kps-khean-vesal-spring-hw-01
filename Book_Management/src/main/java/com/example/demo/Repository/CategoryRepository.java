package com.example.demo.Repository;

import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Repository.Dto.CategoryDto;
import com.example.demo.Utilities.Pagination;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @Insert("INSERT INTO tbl_category (id, title) " +
            "VALUES (#{id}, #{title})")
    boolean insert(CategoryDto categoryDto);
    @Update("UPDATE tbl_category SET title=#{title}")
    boolean update(CategoryDto categoryDto);
    @Select("SELECT * FROM tbl_category ORDER BY id ASC LIMIT #{pagination.limit} OFFSET #{pagination.offset}")
    List<CategoryDto>getAllData(@Param("pagination")Pagination pagination);
    @Delete("DELETE FROM tbl_category WHERE id=#{id}")
    boolean delete(int id);
    @Select("SELECT id FROM tbl_category WHERE id=#{id}")
    CategoryDto findCode(int id);

    @Select("SELECT COUNT(id) FROM tbl_category")
    int countAllCategory();
}
