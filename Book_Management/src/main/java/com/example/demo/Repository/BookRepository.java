package com.example.demo.Repository;

import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Repository.Dto.CategoryDto;
import com.example.demo.Utilities.Pagination;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    @Insert("INSERT INTO tbl_book (id, title, author, description, thumbnail, category_id) " +
            "VALUES (#{id}, #{title}, #{author}, #{description}, #{thumbnail}, #{categoryId})")
    boolean insert(BookDto bookDto);
    @Update("UPDATE tbl_book SET id=#{id},title=#{title},author=#{author},description=#{description}, thumbnail=#{thumbnail}, category_id=#{categoryId} WHERE id=#{id}")
    boolean update(BookDto bookDto);

    @Select("SELECT * FROM tbl_book ORDER BY id ASC LIMIT #{pagination.limit} OFFSET #{pagination.offset}")

//    @Results({
//            @Result(column = "category_id" ,property = "category",many = @Many(select = "selectCate")),
//    })
//    @Select("SELECT * FROM tbl_category WHERE id=#{category_id}")
//    CategoryDto selectCate(int category_id);

    List<BookDto> getAllData(@Param("pagination") Pagination pagination);
    @Select("SELECT * FROM tbl_book WHERE title=#{title}")
    BookDto SearchBookById(String title);
    @Delete("DELETE FROM tbl_book WHERE id=#{id}")
    boolean delete(int id);
    @Select("SELECT id FROM tbl_book WHERE id=#{id}")
    BookDto findCode(int id);
    @Select("SELECT * FROM tbl_book WHERE id=#{id}")
    BookDto findOne(int id);
    @Select("SELECT COUNT(id) FROM tbl_book")
    int countAllBook();
}
