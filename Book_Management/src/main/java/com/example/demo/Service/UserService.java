package com.example.demo.Service;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UserService {

    List<UserDetails> loadUserByUserName(String email);
}
