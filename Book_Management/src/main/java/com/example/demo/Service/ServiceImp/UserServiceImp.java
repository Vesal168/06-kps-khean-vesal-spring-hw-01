package com.example.demo.Service.ServiceImp;

import com.example.demo.Repository.UserRepository;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    private UserRepository repository;

    public List<UserDetails> loadUserByUserName(String email){

        return (List<UserDetails>) repository.loadUserByEmail(email);
    }

}
