package com.example.demo.Service;

import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Repository.Dto.CategoryDto;
import com.example.demo.Utilities.Pagination;

import java.awt.print.Book;
import java.util.List;

public interface CategoryService {
    CategoryDto insert(CategoryDto categoryDto);
    List<CategoryDto> getAllData(Pagination pagination);
    CategoryDto update(CategoryDto categoryDto);
    boolean delete(int id);
    List<CategoryDto> findAll();
    List<CategoryDto> pagination();
    CategoryDto findCode(int id);

    int countAllCategory();
}
