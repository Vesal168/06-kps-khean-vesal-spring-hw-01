package com.example.demo.Service;

import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Utilities.Pagination;

import java.util.List;

public interface BookService {
    BookDto insert(BookDto bookDto);
    List<BookDto> getAllData(Pagination pagination);
    BookDto update(BookDto bookDto);
    boolean delete(int id);
    BookDto findCode(int id);
    BookDto findOne(int id);
    List<BookDto> searchFilter(String title);
    List<BookDto> pagination();
    BookDto fileUpload(BookDto bookDto);
    int countAllBook();
    BookDto SearchBookById(String title);
}
