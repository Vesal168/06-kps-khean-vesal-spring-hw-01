package com.example.demo.Service.ServiceImp;

import com.example.demo.Repository.CategoryRepository;
import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Repository.Dto.CategoryDto;
import com.example.demo.Service.CategoryService;
import com.example.demo.Utilities.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    private CategoryRepository categoryRepository;
    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDto insert(CategoryDto categoryDto) {
        boolean isInserted =categoryRepository.insert(categoryDto);
        if(isInserted)
            return categoryDto;
        else
            return null;
    }

    @Override
    public List<CategoryDto> getAllData(Pagination pagination) {
        return categoryRepository.getAllData(pagination);
    }

    @Override
    public CategoryDto update(CategoryDto categoryDto) {
        boolean isUpdated =categoryRepository.update(categoryDto);
        if(isUpdated)
            return categoryDto;
        else
            return null;
    }

    @Override
    public boolean delete(int id) {
        return categoryRepository.delete(id);
    }

    @Override
    public CategoryDto findCode(int id) {
        return categoryRepository.findCode(1);
    }

    @Override
    public List<CategoryDto> findAll() {
        return null;
    }

    @Override
    public List<CategoryDto> pagination() {
        return null;
    }

    @Override
    public int countAllCategory() {
        return categoryRepository.countAllCategory();
    }
}
