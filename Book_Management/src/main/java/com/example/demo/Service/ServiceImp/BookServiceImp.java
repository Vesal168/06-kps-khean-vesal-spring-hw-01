package com.example.demo.Service.ServiceImp;

import com.example.demo.Repository.BookRepository;
import com.example.demo.Repository.Dto.BookDto;
import com.example.demo.Service.BookService;
import com.example.demo.Utilities.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private BookRepository bookRepository;
    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookDto insert(BookDto bookDto) {
        boolean isInserted =bookRepository.insert(bookDto);
        if(isInserted)
            return bookDto;
        else
            return null;
    }

    @Override
    public List<BookDto> getAllData(Pagination pagination) {
        return bookRepository.getAllData(pagination);
    }

    @Override
    public BookDto update(BookDto bookDto) {
        boolean isUpdated =bookRepository.update(bookDto);
        if(isUpdated)
            return bookDto;
        else
            return null;
    }

    @Override
    public boolean delete(int id) {
        return bookRepository.delete(id);
    }

    @Override
    public BookDto findCode(int id) {
        return bookRepository.findCode(id);
    }

    @Override
    public BookDto findOne(int id) {
        return bookRepository.findOne(id);
    }

    @Override
    public List<BookDto> searchFilter(String title) {
        return null;
    }

    @Override
    public List<BookDto> pagination() {
        return null;
    }

    @Override
    public BookDto fileUpload(BookDto bookDto) {
        return null;
    }

    @Override
    public int countAllBook() {
        return bookRepository.countAllBook();
    }

    @Override
    public BookDto SearchBookById(String title) {
        return bookRepository.SearchBookById(title);
    }
}
